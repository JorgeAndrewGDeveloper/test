package com.jorge.springboot.test.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jorge.springboot.test.app.models.JobModel;
import com.jorge.springboot.test.app.services.JobService;

@RestController
@RequestMapping("/job")
public class JobController {

	@Autowired
	JobService jobService;
	
	@GetMapping("/{id}")
	public JobModel getJobById(@PathVariable Long id) {
		return jobService.getJobById(id);
	}
}
