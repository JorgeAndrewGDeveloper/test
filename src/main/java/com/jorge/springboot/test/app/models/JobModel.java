package com.jorge.springboot.test.app.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name = "jobs")
public class JobModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Long id;
	private String name;
	private Double salary;
	
	@OneToMany(mappedBy = "job")
	private Set<EmployeeModel> employees = new HashSet<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}
	
	public Set<EmployeeModel> getEmployees() {
		return employees;
	}
	
	public void setEmployees(Set<EmployeeModel> employees) {
		this.employees = employees;
		for(EmployeeModel employee : employees) {
			employee.setJob(this);
		}
	}
}