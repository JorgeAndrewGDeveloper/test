package com.jorge.springboot.test.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jorge.springboot.test.app.models.EmployeeWorkedHourModel;
import com.jorge.springboot.test.app.repocitories.EmployeeRepository;
import com.jorge.springboot.test.app.services.EmployeeWorkedHourService;

@RestController
@RequestMapping("/employee-worked-hour")
public class EmployeeWorkedHourController {
	
	@Autowired
	EmployeeWorkedHourService employeeWorkedHourService;
	
	@Autowired
	EmployeeRepository employeeRepository;

	@PostMapping()
	public EmployeeWorkedHourModel storeEmployeeWorkedHour(@RequestBody EmployeeWorkedHourModel employeeWorkedHourModel) {
		return employeeWorkedHourService.storeEmployeeWorkedHour(employeeWorkedHourModel);
	}
}
