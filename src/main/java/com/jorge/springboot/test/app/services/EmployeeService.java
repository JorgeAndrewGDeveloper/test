package com.jorge.springboot.test.app.services;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jorge.springboot.test.app.models.EmployeeModel;
import com.jorge.springboot.test.app.repocitories.EmployeeRepository;
import com.jorge.springboot.test.app.repocitories.EmployeeWorkedHourRepository;

@Service
public class EmployeeService {

	@Autowired
	EmployeeRepository employeeRepository;
	
	@Autowired
	EmployeeWorkedHourRepository employeeWorkedHourRepository;
	
	public EmployeeModel storeEmployee(EmployeeModel employee) {
		return (EmployeeModel) employeeRepository.save(employee);
	}
	
	/*public EmployeeModel getEmployeeSalaryBetweenDate(
			Long id,
			Date initialDate,
			Date finalDate) {
		
	}*/
}
