package com.jorge.springboot.test.app.models;

import java.util.Date;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
@Table(name = "employee_worked_hours")
public class EmployeeWorkedHourModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Long id;
	
	private Integer worked_hours;
	private Date worked_date;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "employee_id", nullable = false)
	@JsonProperty(access = Access.WRITE_ONLY)
	private EmployeeModel employee;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getWorked_hours() {
		return worked_hours;
	}

	public void setWorked_hours(Integer worked_hours) {
		this.worked_hours = worked_hours;
	}

	public EmployeeModel getEmployee() {
		return employee;
	}

	public void setEmployee(EmployeeModel employee) {
		this.employee = employee;
	}

	public Date getWorked_date() {
		return worked_date;
	}

	public void setWorked_date(Date worked_date) {
		this.worked_date = worked_date;
	}

	public EmployeeWorkedHourModel(Long id, Integer worked_hours, EmployeeModel employee) {
		super();
		this.id = id;
		this.worked_hours = worked_hours;
		this.employee = employee;
	}

	public EmployeeWorkedHourModel() {
		super();
		// TODO Auto-generated constructor stub
	}

}

