package com.jorge.springboot.test.app.repocitories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jorge.springboot.test.app.models.JobModel;

public interface JobRepository extends JpaRepository<JobModel, Long> {

}
