package com.jorge.springboot.test.app.controllers;


import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jorge.springboot.test.app.models.EmployeeModel;
import com.jorge.springboot.test.app.services.EmployeeService;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

	@Autowired
	EmployeeService employeeService;
	
	@PostMapping()
	public EmployeeModel storeEmployee(@RequestBody EmployeeModel employee) {
		return employeeService.storeEmployee(employee);
	}
	
	/*@GetMapping("/{id}/{initial_date}/{final_date}")
	public EmployeeModel getEmployeeSalaryBetweenDate(
			@PathVariable Long id,
			@PathVariable Date initialDate,
			@PathVariable Date finalDate) {
		return employeeService.getEmployeeSalaryBetweenDate(id, initialDate, finalDate);
	}*/
}
