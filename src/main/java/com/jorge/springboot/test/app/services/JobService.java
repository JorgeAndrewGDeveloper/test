package com.jorge.springboot.test.app.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jorge.springboot.test.app.models.JobModel;
import com.jorge.springboot.test.app.repocitories.EmployeeRepository;
import com.jorge.springboot.test.app.repocitories.JobRepository;

@Service
public class JobService {

	@Autowired
	JobRepository jobRepository;
	
	@Autowired
	EmployeeRepository employeeRepository;
	
	public JobModel getJobById(Long id) {
		Optional<JobModel> jobOptional = jobRepository.findById(id);
		
		if(!jobOptional.isPresent()) {
			
		}
		
		return jobOptional.get();
	}
}
