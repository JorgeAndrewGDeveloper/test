package com.jorge.springboot.test.app.repocitories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jorge.springboot.test.app.models.EmployeeWorkedHourModel;

public interface EmployeeWorkedHourRepository extends JpaRepository<EmployeeWorkedHourModel, Long>{

}
