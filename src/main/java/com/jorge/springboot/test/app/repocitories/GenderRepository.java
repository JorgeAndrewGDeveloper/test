package com.jorge.springboot.test.app.repocitories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jorge.springboot.test.app.models.GenderModel;

public interface GenderRepository extends JpaRepository<GenderModel, Long> {

}
