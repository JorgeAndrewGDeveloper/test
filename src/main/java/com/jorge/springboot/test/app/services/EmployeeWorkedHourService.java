package com.jorge.springboot.test.app.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jorge.springboot.test.app.models.EmployeeModel;
import com.jorge.springboot.test.app.models.EmployeeWorkedHourModel;
import com.jorge.springboot.test.app.repocitories.EmployeeRepository;
import com.jorge.springboot.test.app.repocitories.EmployeeWorkedHourRepository;

@Service
public class EmployeeWorkedHourService {
	

	@Autowired
	EmployeeWorkedHourRepository employeeWorkedHourRepository;
	
	@Autowired
	EmployeeRepository employeeRepository;
	
	public EmployeeWorkedHourModel storeEmployeeWorkedHour(EmployeeWorkedHourModel employeeWorkedHourModel) {
		Optional<EmployeeModel> employeeOptional = employeeRepository
				.findById(employeeWorkedHourModel
						.getEmployee()
						.getId());
		
		if(!employeeOptional.isPresent()) {
			
		}
		
		employeeWorkedHourModel.setEmployee(employeeOptional.get());
		
		return employeeWorkedHourRepository.save(employeeWorkedHourModel);
	}
}
