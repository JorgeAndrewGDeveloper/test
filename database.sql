CREATE DATABASE springboot_test;

USE springboot_test;

CREATE TABLE genders (
	id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL);
    
INSERT INTO genders (
	name) VALUES 
    ('Masculino'),
    ('Femenino'),
    ('No especificado');

CREATE TABLE jobs (
	id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    salary DOUBLE(9,2) NOT NULL);
    
INSERT INTO jobs (
	name, salary) VALUES 
    ('Desarrollador', 35000),
    ('Lider Técnico', 45000),
    ('Project Manajer', 55000);

CREATE TABLE employees (
	id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    gender_id BIGINT NOT NULL,
    job_id BIGINT NOT NULL,
    name VARCHAR(255) NOT NULL,
    last_name VARCHAR(255) NOT NULL,
    birthdate DATE NOT NULL,
    FOREIGN KEY(gender_id) REFERENCES genders(id),
    FOREIGN KEY(job_id) REFERENCES jobs(id));
    
CREATE TABLE employee_worked_hours (
	id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    employee_id BIGINT NOT NULL,
    worked_hours INT NOT NULL,
    worked_date DATE NOT NULL,
    FOREIGN KEY(employee_id) REFERENCES employees(id));